import React, { Component } from 'react'; 

class LeftpanelOptions extends Component {

    handleChange = (e) => {
        let { currentTarget:input } = e; 
        let options = {...this.props.options}; 
        options[input.name] = input.value; 
        this.props.handleOptionChange(options); 
    }; 

    render() {
        let { sections, options, ordersBy} = this.props;  
        console.log(options); 

        return <React.Fragment>
            <div className="row">
                <label className="border py-2 fw-bold ">Order By</label>
                <select 
                class="form-select"
                id="order-by"
                name="order-by" 
                value={options['order-by']}
                onChange={this.handleChange} >
                    <option selected value="">Order By</option>
                    {ordersBy.map(ele => 
                    <option>{ele}</option>
                    )}
                </select>
            </div>
            <br />
            <br />
            <div className="row">
            <div className="col-12 border fw-bold py-2 bg-light">Sections</div>
            {sections.map(ele => 
              <div className="col-12 border py-2">
                <div class="form-check">
               <input 
               class="form-check-input" 
               type="radio" 
               name="section" 
               id="section" 
               value={ele} 
               checked={options.section === ele || false}
               onChange={this.handleChange}
               />
               <label class="form-check-label">
                 {ele.charAt(0).toUpperCase() + ele.slice(1)}
               </label>
               </div>
             </div>
             )}
            </div>
        </React.Fragment>
    }
}
export default LeftpanelOptions;