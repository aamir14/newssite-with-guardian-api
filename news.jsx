import React, { Component } from 'react';
import { Link } from "react-router-dom"; 
import LeftPanelOptions from './leftPanelOptions';
import http from './httpService';   
import queryString from 'query-string'; 

class News extends Component {

    state = {
        datas: {},
        sections: ['business', 'technology', 'politics', 'lifeStyle'],  
        ordersBy: ['newest', 'oldest', 'relevance'], 
        q: '', 
    }; 

    handleChange = (e) => {
        let { currentTarget:input } = e; 
        let s1 = {...this.state}; 
        s1[input.name] = input.value; 
        this.setState(s1); 
    }; 

    handleSearch = () => {
        let s1 = {...this.state}; 
        let queryParams = queryString.parse(this.props.location.search); 
        queryParams.q = this.state.q;
        let page = "1"; 
        queryParams.page= page; 
        this.callURL(`/home`, queryParams); 
    }

    async fetchData() { 
        let queryParams = queryString.parse(this.props.location.search);
        let searchStr = this.makeSearchString(queryParams); 
        console.log(searchStr); 
        let response1 = await http.get(`?${searchStr}&api-key=test`); 
        let { data } = response1; 
        let { response } = data; 
        console.log('searcstr', searchStr); 
        this.setState({datas: response}); 
    }; 

    componentDidMount() {
        this.fetchData();     
    }; 

    componentDidUpdate(prevProps){
        if(prevProps !== this.props) this.fetchData(); 
    }; 

    handlePage = (inc) => {
        console.log('innn', inc); 
        let queryParams = queryString.parse(this.props.location.search); 
        let { page="1" } = queryParams; 
        let newPage = +page + inc; 
        console.log(newPage); 
        queryParams.page = newPage;  
        this.callURL(`/home`, queryParams); 
    }; 

    handleOptionChange = (options) => {
        options.page = "1"; 
        this.callURL("/home", options); 
    }

    callURL = (url, options) => {
        console.log("I am in"); 
        let searchString = this.makeSearchString(options); 
        console.log('callURL String', searchString)
        this.props.history.push({
            pathname: url, 
            search: searchString
        }); 
    }; 

    makeSearchString = (options) => {
        let {q, page="1", section} = options; 
        let searchStr = ""; 
        searchStr = this.addToQueryString(searchStr,"q", q);
        searchStr = this.addToQueryString(searchStr,"page", page); 
        searchStr = this.addToQueryString(searchStr,"section", section);
        searchStr = this.addToQueryString(searchStr,"order-by", options['order-by']); 
        // console.log('searchStr', searchStr); 
        return searchStr; 
    }; 

    addToQueryString = (str, paramName, paramValue) =>
        paramValue ? str ? `${str}&${paramName}=${paramValue}` : `${paramName}=${paramValue}` : str; 

    render() {
        let { datas, sections, ordersBy,q } = this.state; 
        let {currentPage, pageSize, pages,total, results=[]} = datas; 
        console.log('results', results); 
        console.log('datas', datas); 
        let queryParams =queryString.parse(this.props.location.search);
        let {q:genre } = queryParams; 

        return <React.Fragment>
            <div className="row py-4">
                <div className="col-2">
                    <LeftPanelOptions sections={sections} ordersBy={ordersBy} options={queryParams} handleOptionChange={this.handleOptionChange} />
                </div>
                <div className="col-10 px-5">
                <form class="row">
                    <div class="col">
                    <input 
                    type="text" 
                    class="form-control" 
                    id="q"
                    name="q"
                    value={q ? q : genre} 
                    placeholder="Enter Search Text" 
                    onChange={this.handleChange} />
                    </div>
                    <div class="col-auto">
                    <button type="submit" class="btn btn-light mb-3" onClick={this.handleSearch}>Submit</button>
                    </div>
                </form>
                {genre ? 
                <React.Fragment>
                <label>
                {currentPage > 1 ? (pageSize * (currentPage - 1) + 1 )  : currentPage} to {pages === currentPage ? total : pageSize * currentPage} of {total}
                </label> <br />
                <div className="row row-cols-1 row-cols-md-3 g-4">
                    {results.map((ele,index) => 
                        <div className="col" key={index}>
                            <div className="card" style={{backgroundColor: "#cbeff6"}}>
                           <div className="card-body text-center">
                           <label>{ele.webTitle}</label><br />
                            <label className="fw-bold">Source : {ele.sectionName}</label><br />
                            <label className="fw-bold">{ele.webPublicationDate}</label> <br />
                            <label className="fw-bold"><a href={ele.webUrl} className="link" target="_blank">Preview</a></label>
                           </div>
                        </div>
                        </div>
                        )}
                 </div>
                 
                 <div className="row py-3">
                <div className="col-2">
                    {currentPage >=2 ? 
                    <button className="btn btn-danger btn-sm" onClick={() => this.handlePage(-1)}>Prev</button> : ''}
                </div>
                <div className="col-8"></div>
                <div className="col-2 text-end">
                    {currentPage < pageSize  ? 
                     <button className="btn btn-danger btn-sm" onClick={() => this.handlePage(1)}>Next</button> : ''}
                </div>
            </div>
            </React.Fragment>
             : ''}
                </div>
            </div>
        </React.Fragment>
    }
}
export default News;