import axios from 'axios';

const baseURL = "https://content.guardianapis.com/search"; 

function get(url) {
    return axios.get(baseURL + url); 
}

export default {
    get, 
}; 
