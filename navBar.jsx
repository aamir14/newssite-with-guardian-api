import React, { Component } from 'react'; 
import { Link } from 'react-router-dom'; 

class NavBar extends Component {

    render() {
        return <React.Fragment>
            <nav className="navbar navbar-expand-sm navbar-dark bg-danger">
  <div className="container-fluid">
    <Link className="navbar-brand fw-bold text-dark" to="/" >NewsSite</Link>
    <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
      <span className="navbar-toggler-icon"></span>
    </button>
    <div className="collapse navbar-collapse" id="navbarNavAltMarkup">
      <div className="navbar-nav">
        <Link className="nav-link" to="/home?q=sports&page=1">Sports</Link>
        <Link className="nav-link" to="/home?q=cricket&page=1">Cricket</Link>
        <Link className="nav-link" to="/home?q=movies&page=1">Movies</Link>
        <Link className="nav-link" to="/home?q=education&page=1">Education</Link>
      </div>
    </div>
  </div>
</nav>
        </React.Fragment>
    }
}
export default NavBar;