import React, { Component } from 'react'; 
import { Switch, Route, Redirect } from 'react-router-dom'; 
import NavBar from './navBar'; 
import News from './news'; 

class MainComponent extends Component {

    render() {
        return <React.Fragment>
            <NavBar />
            <div className="container">
            <Switch>
                <Route path="/home" component={News} />
                <Redirect from='/' to='/home' />
            </Switch>
            </div>
        </React.Fragment>
    }
}
export default MainComponent;